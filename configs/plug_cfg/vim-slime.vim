" I don't want vim to automatically set the pane
" https://github.com/jgdavey/tslime.vim/pull/28
let g:tslime_autoset_pane = 0
