" TODO - rename this file to ale config or something
" let g:ale_linters = {
" \   'javascript': ['eslint'],
" \   'ruby': ['rubocop'],
" \}

let g:ale_fixers = {
 \ 'javascript': ['eslint']
 \ }

" Ale config for linting
let g:ale_sign_error = '☹️' " Less aggressive than the default '>>'
let g:ale_sign_warning = '.'
" let g:ale_lint_on_enter = 0 " Less distracting when opening a new file
" " Set specific linters
"
" let g:ale_sign_column_always = 1
" let g:ale_change_sign_column_color = 1
" let g:ale_detail_to_floating_preview = 1
"
" " Only run linters named in ale_linters settings.
" " let g:ale_linters_explicit = 1
"
" autocmd BufWritePost *.js,*.jsx AsyncRun -post=checktime ./node_modules/.bin/eslint --fix %
